<?php

namespace Framework;

use ArrayAccess;
use InvalidArgumentException;
use Serializable;

/**
 * Class FlashBag
 * @package Framework
 */
final class FlashBag implements ArrayAccess, Serializable
{
    /** @var array */
    protected array $data = [];

    /** FlashBag constructor. */
    public function __construct()
    {
    }

    /** Clears the entire contents of the FlashBag */
    public function clear(): void
    {
        $this->data = [];
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed|null Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset): ?mixed
    {
        if (false === $this->offsetExists($offset)) {
            return null;
        }

        $value = $this->data[$offset];
        $this->offsetUnset($offset);

        return $value;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset): bool
    {
        if ($this->data === null) {
            return false;
        }

        return array_key_exists($offset, $this->data);
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value): void
    {
        if (null === $offset) {
            throw new InvalidArgumentException("\$offset cannot be null.");
        }
        $this->data[$offset] = $value;
    }

    /** @return array */
    public function toArray(): array
    {
        $data = $this->data;
        $this->clear();

        return $data;
    }

    /** @return int */
    public function count(): int
    {
        return count($this->data);
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize(): string
    {
        return serialize($this->data);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     * @noinspection PhpDocSignatureInspection
     */
    public function unserialize($serialized): void
    {
        $this->data = unserialize($serialized);
    }
}
