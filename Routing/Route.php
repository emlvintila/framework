<?php

namespace Framework\Routing;

/**
 * Class Route
 * @package Framework\Routing
 */
class Route
{
    /** @var string */
    protected string $path;
    /** @var string */
    protected string $controller;
    /** @var string */
    protected string $action;
    /** @var string */
    protected string $method;

    /**
     * Route constructor.
     * @param string $path
     * @param string $controller
     * @param string $action
     * @param string $method
     */
    public function __construct(string $path, string $controller, string $action, string $method)
    {
        $this->path = $path;
        $this->controller = $controller;
        $this->action = $action;
        $this->method = $method;
    }

    /** @return string */
    public function getPath(): string
    {
        return $this->path;
    }

    /** @return string */
    public function getController(): string
    {
        return $this->controller;
    }

    /** @return string */
    public function getAction(): string
    {
        return $this->action;
    }

    /** @return string */
    public function getMethod(): string
    {
        return $this->method;
    }
}
