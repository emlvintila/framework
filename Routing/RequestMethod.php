<?php

namespace Framework\Routing;

/**
 * Class RequestMethod
 * @package Framework\Routing
 */
class RequestMethod
{
    /**
     * RequestMethod constructor.
     */
    private function __construct()
    {
    }

    /** @var string */
    public const GET = 'GET';
    /** @var string */
    public const POST = 'POST';
    /** @var string */
    public const PUT = 'PUT';
    /** @var string */
    public const HEAD = 'HEAD';
    /** @var string */
    public const DELETE = 'DELETE';
    /** @var string */
    public const PATCH = 'PATCH';
    /** @var string */
    public const OPTIONS = 'OPTIONS';
}
