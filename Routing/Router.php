<?php

namespace Framework\Routing;

use Framework\DependencyInjection\DependencyInjectionService;
use Framework\Utils;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;
use RuntimeException;

/**
 * Class Router
 * @package Framework\Routing
 */
class Router
{
    /** @var Route */
    protected Route $fallback;
    /** @var Route[] */
    protected array $routes = [];
    /** @var DependencyInjectionService */
    protected DependencyInjectionService $di;

    /**
     * Router constructor.
     * @param DependencyInjectionService $dependencyInjectionService
     */
    public function __construct(DependencyInjectionService $dependencyInjectionService)
    {
        $this->di = $dependencyInjectionService;
    }

    /** @param Route $route */
    public function register(Route $route): void
    {
        $this->routes[] = $route;
    }

    /** @param Route $fallback */
    public function setFallback(Route $fallback): void
    {
        $this->fallback = $fallback;
    }

    /** @param string $request */
    public function handleRequest(string $request)
    {
        $matches = [];
        foreach ($this->routes as $route) {
            if ($route->getMethod() === $_SERVER['REQUEST_METHOD']) {
                if (preg_match($route->getPath(), $request, $matches)) {
                    // $matches[0] will always be equal to $request, so we just shift it off
                    array_shift($matches);
                    $this->useRoute($route, $matches);
                    // Finally, we return from the function because we don't want our request to be handled more than once
                    return;
                }
            }
        }

        $this->useRoute($this->fallback, [$request]);
    }

    /**
     * @param Route $route
     * @param string[] $args
     */
    protected function useRoute(Route $route, array $args)
    {
        try {
            $class = new ReflectionClass($route->getController());
        } catch (ReflectionException $exception) {
            throw new RuntimeException(
                "Route \"{$route->getPath()}\" specifies an non-existent controller {$route->getController()}");
        }
        try {
            $method = $class->getMethod($route->getAction());
        } catch (ReflectionException $exception) {
            throw new RuntimeException("Method {$route->getController()}::{$route->getAction()} does not exist.");
        }
        $method_args = $this->constructMethodArgumentsFromArray($method, $args);
        $controller = $this->di->inject($class);
        $method->invokeArgs($controller, $method_args);
    }

    /**
     * @param ReflectionMethod $method
     * @param array $array
     * @return array
     */
    protected function constructMethodArgumentsFromArray(ReflectionMethod $method, array &$array): array
    {
        $parameters = $method->getParameters();
        $args = [];
        foreach ($parameters as $parameter) {
            $args[] = $this->constructArgumentFromArray($parameter, $array);
        }

        return $args;
    }

    /**
     * @param ReflectionParameter $parameter
     * @param array $array
     * @return bool|float|int|object|string|null
     */
    protected function constructArgumentFromArray(ReflectionParameter $parameter, array &$array): object|int|bool|float|string|null
    {
        $type = $parameter->getType();
        // If the parameter wasn't declared with any type, just return the next element from the array
        if ($type === null) {
            return array_shift($array);
        }

        $class = $parameter->getClass();
        // If the parameter is a class type
        if ($class !== null) {
            $constructor = $class->getConstructor();
            // make another call that will actually call this method
            $args = $this->constructMethodArgumentsFromArray($constructor, $array);
            return $class->newInstanceArgs($args);
        }

        // We've run out of $array elements
        if (count($array) === 0) // But we can pass null if we're allowed to
        {
            if ($parameter->allowsNull()) {
                return null;
            } else {
                throw new RuntimeException(
                    "We can't construct the '{$parameter->getName()}' in {$parameter->getDeclaringClass()->getName()}::{$parameter->getDeclaringFunction()->getName()} because we've ran out of array elements.");
            }
        }

        // If the parameter is a primitive type, just do a cast
        try {
            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
            return Utils::castToPrimitive(array_shift($array), $type->getName());
        } catch (InvalidArgumentException $exception) {
            throw new RuntimeException(
                "We don't know how to construct the '{$parameter->getName()}' parameter in " .
                "{$parameter->getDeclaringClass()->getName()}::{$parameter->getDeclaringFunction()->getName()}."
            );
        }
    }
}
