<?php

namespace Framework\Logging;

use Framework\Utils;
use SplFileObject;

/**
 * Class FileLogger
 * @package Framework\Logging
 */
class FileLogger extends Logger
{
    /** @var SplFileObject */
    protected SplFileObject $file;

    /**
     * Logger constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $dir = dirname($filename);
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        $this->file = new SplFileObject($filename, 'a+');
    }


    /**
     * @param int $severity
     * @param array $args
     */
    public function log(int $severity, ...$args): void
    {
        switch ($severity) {
            default:
            case self::LOG_INFO:
                $type_str = "INFO";
                break;
            case self::LOG_WARN:
                $type_str = "WARN";
                break;
            case self::LOG_ERROR:
                $type_str = "ERROR";
                break;
        }
        $now = date_create()->format("d-m-Y H:i:s");

        $args = Utils::arrayFlatten($args);
        $message = join(PHP_EOL, $args);
        $this->file->fwrite("[{$type_str}][{$now}] {$message}" . PHP_EOL);
        $this->file->fflush();
    }
}
