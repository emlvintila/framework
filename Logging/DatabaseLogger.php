<?php

namespace Framework\Logging;

use Framework\Persistence\Sql\DatabaseConnection;

/**
 * Class DatabaseLogger
 * @package Framework\Logging
 */
abstract class DatabaseLogger extends Logger
{
    /** @var DatabaseConnection */
    protected DatabaseConnection $connection;

    /**
     * DatabaseLogger constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param int $severity
     * @param array $args
     * @return bool
     */
    abstract public function log(int $severity, ...$args): bool;
}
