<?php

namespace Framework\Logging;

/**
 * Class Logger
 * @package Framework\Logging
 */
abstract class Logger
{
    /** @var int */
    public const LOG_INFO = 0;
    /** @var int */
    public const LOG_WARN = 1;
    /** @var int */
    public const LOG_ERROR = 2;
    /** @var int */
    public const LOG_FATAL = 3;
    /** @var int[] */
    protected const LOG_LEVELS = [self::LOG_INFO, self::LOG_WARN, self::LOG_ERROR, self::LOG_FATAL];

    /**
     * @param int $severity
     * @param array $args
     */
    abstract public function log(int $severity, ...$args);

    /** @param array $args */
    public function info(...$args): void
    {
        $this->log(self::LOG_INFO, $args);
    }

    /** @param array $args */
    public function warn(...$args)
    {
        $this->log(self::LOG_WARN, $args);
    }

    /** @param array $args */
    public function error(...$args)
    {
        $this->log(self::LOG_ERROR, $args);
    }

    /** @param mixed ...$args */
    public function fatal(...$args)
    {
        $this->log(self::LOG_FATAL, $args);
    }
}
