<?php

namespace Framework\View;

use Framework\ViewModel\ViewModel;
use LogicException;

/**
 * Class View
 * @package Framework\View
 */
class View
{
    /** @var View */
    protected static View $current;
    /** @var ViewModel */
    protected static ViewModel $view_model;
    /** @var string[] */
    protected array $sections;
    /** @var string[] */
    protected array $section_stack = [];
    /** @var string|null */
    protected ?string $base_view;

    /**
     * @param string|null $base_view
     */
    public function __construct(string $base_view = null)
    {
        if (isset(static::$current) && static::$current !== null) {
            $this->sections = static::$current->sections;
        } else {
            $this->sections = [];
        }

        $this->base_view = $base_view;
        static::$current = $this;
    }

    /** @return View */
    public static function getCurrent(): View
    {
        return static::$current;
    }

    /** @return ViewModel */
    public static function getViewModel(): ?ViewModel
    {
        return isset(static::$view_model) ? static::$view_model : new ViewModel();
    }

    /**
     * @param ViewModel $view_model
     * @return ViewModel
     */
    public static function setViewModel(ViewModel $view_model): ViewModel
    {
        return static::$view_model = $view_model;
    }

    /**
     * @param string $section_name
     * @param bool $required
     * @return string
     */
    public function defineSection(string $section_name, bool $required = false): string
    {
        if ($required && false === array_key_exists($section_name, $this->sections)) {
            throw new LogicException("$section_name was not implemented by extending view.");
        }

        return $this->sections[$section_name] ?? '';
    }

    /** @param string $section_name */
    public function beginSection(string $section_name): void
    {
        /*if (array_key_exists($section_name, $this->sections))
            throw new InvalidArgumentException("Section $section_name already implemented.");*/

        array_push($this->section_stack, $section_name);
        ob_start();
    }

    public function endSection(): void
    {
        if (count($this->section_stack) === 0) {
            throw new LogicException("There was no started section.");
        }

        $this->sections[array_pop($this->section_stack)] = ob_get_clean();
    }

    /** @return string|null */
    public function getBaseView(): ?string
    {
        return $this->base_view;
    }
}
