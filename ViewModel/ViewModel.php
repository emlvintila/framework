<?php

namespace Framework\ViewModel;

use Framework\Session\Session;

/**
 * Class ViewModel
 * @package Framework\ViewModel
 */
class ViewModel
{
    /** @return string[] */
    public static function getErrors(): array
    {
        $session = Session::getInstance();
        if ($session->hasErrors()) {
            return $session->getErrorsBag()->toArray();
        }

        return [];
    }

    /** @return string[] */
    public static function getSuccess(): array
    {
        $session = Session::getInstance();
        if ($session->hasSuccess()) {
            return $session->getSuccessBag()->toArray();
        }

        return [];
    }
}
