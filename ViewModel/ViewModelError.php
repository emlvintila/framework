<?php

namespace Framework\ViewModel;

use Serializable;

/**
 * Class ViewModelError
 * @package Framework\ViewModel
 */
class ViewModelError implements Serializable
{
    /** @var string */
    protected string $message;
    /** @var int|null */
    protected ?int $error_code;

    /**
     * ViewModelError constructor.
     * @param string $message
     * @param int|null $error_code
     */
    public function __construct(string $message, ?int $error_code = null)
    {
        $this->message = $message;
        $this->error_code = $error_code;
    }

    /** @return string */
    public function getMessage(): string
    {
        return $this->message;
    }

    /** @return int|null */
    public function getErrorCode(): ?int
    {
        return $this->error_code;
    }

    /** @return string */
    public function __toString(): string
    {
        $str = '';
        if ($this->error_code !== null) {
            $str .= "($this->error_code) ";
        }
        $str .= $this->message;

        return $str;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize(): string
    {
        return serialize([
            $this->message,
            $this->error_code,
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     * @noinspection PhpDocSignatureInspection
     */
    public function unserialize($serialized)
    {
        [
            $this->message,
            $this->error_code,
        ] = unserialize($serialized);
    }
}
