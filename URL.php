<?php

namespace Framework;

/**
 * Class URL
 * @package Framework
 */
class URL
{
    /**
     * @param string $relative
     * @return string
     */
    public static function fromRoot(string $relative): string
    {
        $host = $_SERVER['HTTP_HOST'];
        $scheme = Utils::isSSL() ? 'https' : 'http';
        $relative = preg_replace('/^\/+/', '', $relative);

        return "{$scheme}://{$host}/{$relative}";
    }
}
