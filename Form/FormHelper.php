<?php

namespace Framework\Form;

use DateTimeInterface;
use Framework\Data\Model\Entity;
use Framework\Session\Session;
use Generator;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Traversable;

/**
 * Class FormHelper
 * @package Framework\Form
 */
class FormHelper
{
    /**
     * @param Entity $entity
     * @param array $entity_properties
     * @param string[][] $input_attributes
     * @return Generator
     * @throws ReflectionException
     */
    public static function getInputs(Entity $entity, array $entity_properties, array $input_attributes = []): Generator
    {
        /** @var FormInput[] $inputs */
        $inputs = [];
        $ref_class = new ReflectionClass($entity);
        foreach ($entity_properties as $prop_name) {
            /** @var ReflectionProperty $ref_prop */
            $ref_prop = null;
            try {
                $ref_prop = $ref_class->getProperty($prop_name);
                $ref_prop->setAccessible(true);
            } catch (ReflectionException $exception) {
                continue;
            }
            $prop_val = $ref_prop->getValue($entity);
            /** @var string $input_name */
            $input_name = $prop_name;
            /** @var string $input_type */
            $input_type = null;
            /** @var mixed $input_value */
            $input_value = null;
            switch (strtolower(gettype($prop_val))) {
                case 'boolean':
                    $input_type = 'checkbox';
                    $input_value = $prop_val;
                    break;
                case 'integer':
                case 'double':
                    $input_type = 'number';
                    $input_value = $prop_val;
                    break;
                case 'string':
                    $input_type = 'text';
                    $input_value = $prop_val;
                    break;
                case 'object':
                    $prop_obj_ref = new ReflectionClass($prop_val);
                    if ($prop_obj_ref->implementsInterface(DateTimeInterface::class)) {
                        /** @var DateTimeInterface $date_time */
                        $date_time = $prop_val;
                        // TODO: Add timezone when browsers start supporting it
                        $dtz_string = $date_time->format('Y-m-d\TH:i:s');
                        $input_value = $dtz_string;
                    }
                    $input_type = 'datetime-local';
                    break;
                case 'array':
                case 'resource':
                case 'null':
                case 'unknown type':
                    throw new InvalidArgumentException("Property $prop_name can't be transformed in an input.");
            }
            $inputs[] = new FormInput($input_name, $input_type, $input_value);
        }

        $input_attr_list = join(' ',
            array_map(fn($attr_name, $attr_val) => is_numeric($attr_name) ? $attr_val : "$attr_name=\"$attr_val\"",
                array_keys($input_attributes), array_values($input_attributes)));
        $class_short_name = $ref_class->getShortName();
        foreach ($inputs as $input) {
            $name = $input->getName();
            $input_type = $input->getType();
            $value = $input->getValue();
            $full_name = "${class_short_name}[$name]";

            $label = implode(' ', array_map(function ($str) {
                $str[0] = strtoupper($str[0]);

                return $str;
            }, explode('_', $name)));


            yield <<<TAG
<label for="$full_name">$label</label>
<input id="$full_name" name="$full_name" type="$input_type" value="$value" $input_attr_list />
TAG;
        }
    }

    /**
     * @param Traversable $inputs
     * @param string $start_tag
     * @param string $end_tag
     * @return string
     */
    public static function renderInputs(Traversable $inputs, string $start_tag = '<div>',
        string $end_tag = '</div>'): string
    {
        $str = '';
        foreach ($inputs as $input) {
            $str .= $start_tag . $input . $end_tag;
        }

        return $str;
    }

    /** @return string */
    public static function renderCsrfTag(): string
    {
        return static::getCsrfTag();
    }

    /** @return string */
    public static function getCsrfTag(): string
    {
        $token = Session::getInstance()->getCsrfToken()->get();

        return "<input type=\"hidden\" name=\"csrf_token\" value=\"${token}\" />";
    }
}
