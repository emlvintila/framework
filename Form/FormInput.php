<?php

namespace Framework\Form;

/**
 * Class FormInput
 * @package Framework\Form
 */
class FormInput
{
    /** @var string */
    protected string $name;
    /** @var string */
    protected string $type;
    /** @var mixed */
    protected $value;

    /**
     * FormInput constructor.
     * @param string $name
     * @param string $type
     * @param mixed $value
     */
    public function __construct(string $name, string $type = 'text', $value = '')
    {
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
    }

    /** @return string */
    public function getName(): string
    {
        return $this->name;
    }

    /** @param string $name */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** @return string */
    public function getType(): string
    {
        return $this->type;
    }

    /** @param string $type */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /** @return mixed */
    public function getValue(): mixed
    {
        return $this->value;
    }

    /** @param mixed $value */
    public function setValue($value): void
    {
        $this->value = $value;
    }
}