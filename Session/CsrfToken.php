<?php

namespace Framework\Session;

use DateInterval;
use DateTimeImmutable;
use Framework\Utils;
use Serializable;

/**
 * Class CsrfToken
 * @package Framework\Session
 */
class CsrfToken implements Serializable
{
    /** @var string */
    protected string $value;
    /** @var DateInterval */
    protected DateInterval $lifetime;
    /** @var DateTimeImmutable */
    protected DateTimeImmutable $last_access;

    /**
     * CsrfToken constructor.
     * @param DateInterval|null $lifetime
     */
    public function __construct(DateInterval $lifetime = null)
    {
        if ($lifetime === null) {
            $lifetime = new DateInterval('PT1H');
        }

        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->lifetime = $lifetime;
        $this->last_access = new DateTimeImmutable();
        $this->generateValue();
    }

    protected function generateValue(): void
    {
        $this->value = Utils::generateCryptoString();
    }

    /**
     * @return string
     */
    public function get(): string
    {
        $now = new DateTimeImmutable();
        if ($now > $this->last_access->add($this->lifetime)) {
            $this->generateValue();
        }
        $this->last_access = $now;

        return $this->value;
    }

    /** @return string */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize(): string
    {
        return serialize([$this->value, $this->last_access, $this->lifetime]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     * @noinspection PhpDocSignatureInspection
     */
    public function unserialize($serialized)
    {
        [$this->value, $this->last_access, $this->lifetime] = unserialize($serialized);
    }
}
