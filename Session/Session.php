<?php

namespace Framework\Session;

use Framework\FlashBag;
use Framework\Traits\Singleton;
use LogicException;

/**
 * Class Session
 * @package Framework\Session
 */
class Session
{
    use Singleton;

    /** @var int|null */
    protected ?int $user_id;
    /** @var FlashBag */
    protected FlashBag $errors;
    /** @var FlashBag */
    protected FlashBag $success;
    /** @var CsrfToken */
    protected CsrfToken $csrf_token;
    /** @var FlashBag */
    protected FlashBag $internal_flash_bag;
    /** @var bool */
    protected bool $from_post;

    /** Session constructor. */
    protected function __construct()
    {
        $this->user_id = $_SESSION['user.id'] ?? null;
        $this->errors = $_SESSION['flashbag.errors'] ??= new FlashBag();
        $this->success = $_SESSION['flashbag.success'] ??= new FlashBag();
        $this->csrf_token = $_SESSION['csrf_token'] ??= new CsrfToken();

        $this->internal_flash_bag = $_SESSION['internal.flashbag'] ??= new FlashBag();
        $this->from_post = $this->internal_flash_bag['from_post'] ?? false;
    }

    /** @return FlashBag */
    public function getErrorsBag(): FlashBag
    {
        return $this->errors;
    }

    /** @return bool */
    public function hasErrors(): bool
    {
        return $this->from_post && $this->errors->count();
    }

    /** @return FlashBag */
    public function getSuccessBag(): FlashBag
    {
        return $this->success;
    }

    /** @return bool */
    public function hasSuccess(): bool
    {
        return $this->from_post && $this->success->count();
    }

    /** @return CsrfToken */
    public function getCsrfToken(): CsrfToken
    {
        return $this->csrf_token;
    }

    /** @return int|null */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /** @param int $user_id */
    public function setUserId(int $user_id): void
    {
        if ($this->user_id !== null) {
            throw new LogicException("User ID is already set.");
        }

        $this->user_id = $_SESSION['user.id'] = $user_id;
    }

    /** @return bool */
    public function isLoggedIn(): bool
    {
        return $this->user_id !== null;
    }

    /** @return bool */
    public function isFromPost(): bool
    {
        return $this->from_post;
    }

    /** @param bool $from_post */
    public function setFromPost(bool $from_post): void
    {
        $this->from_post = $this->internal_flash_bag['from_post'] = $from_post;
    }
}
