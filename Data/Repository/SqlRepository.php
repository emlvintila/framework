<?php

namespace Framework\Data\Repository;

use Framework\Persistence\Sql\Driver\ISqlDriver;

/**
 * Class SqlRepository
 * @package Framework\Data\Repository
 */
abstract class SqlRepository implements IRepository
{
    /** @var ISqlDriver */
    private ISqlDriver $driver;

    /**
     * SqlRepository constructor.
     * @param ISqlDriver $driver
     */
    public function __construct(ISqlDriver $driver)
    {
        $this->driver = $driver;
    }

    /** @return ISqlDriver */
    public function getDriver(): ISqlDriver
    {
        return $this->driver;
    }
}
