<?php

namespace Framework\Data\Model;

/**
 * Interface IEntity
 * @package Framework\Data\Model
 */
interface IEntity
{
    /** @return int */
    public function getId(): int;
}
