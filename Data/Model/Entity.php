<?php

namespace Framework\Data\Model;

/**
 * Class Entity
 * @package Framework\Data\Model
 */
abstract class Entity implements IEntity
{
    /** @var int */
    protected int $id;

    /** @return int */
    public function getId(): int
    {
        return $this->id;
    }
}
