<?php

namespace Framework;

use Exception;
use Framework\DependencyInjection\DependencyInjectionService;
use Framework\Persistence\Sql\DatabaseConnection;
use Framework\Routing\RequestMethod;
use Framework\Routing\Route;
use Framework\Routing\Router;
use Framework\Traits\Singleton;
use Throwable;

/**
 * Class App
 * @package Framework
 */
class App
{
    use Singleton;

    /** @var Environment */
    protected Environment $env;
    /** @var DatabaseConnection[] */
    protected array $database_connections;
    /** @var callable[] */
    protected array $exception_handlers;
    /** @var DependencyInjectionService */
    protected DependencyInjectionService $dependency_injection_service;
    /** @var Router */
    protected Router $router;
    /** @var i18n */
    protected i18n $i18n;

    /** App constructor. */
    protected function __construct()
    {
        session_name("UID");
        session_start();
        $this->env = new Environment(true, true);
        $this->database_connections = [];
        $this->exception_handlers = [];
        $this->dependency_injection_service = new DependencyInjectionService();
        $this->router = new Router($this->dependency_injection_service);
    }

    /** @return Environment */
    public function getEnv(): Environment
    {
        return $this->env;
    }

    /**
     * Shortcut for `App::GetInstance()->getEnv()->isDevelopment()`
     * @return bool
     */
    public static function isDevelopment(): bool
    {
        return static::getInstance()->getEnv()->isDevelopment();
    }

    /**
     * @param string $filename
     * @throws Exception
     */
    public function configureFrom(string $filename): void
    {
        $directory = dirname($filename);
        $config = json_decode(file_get_contents($filename), true);

        $this->env = new Environment(
            $config['env']['isDevelopment'] ?? true,
            $config['env']['useOutputBuffering'] ?? true
        );

        foreach ($config['routing']['routes'] as $route) {
            // escape regex meta characters
            $regex = '/^' . preg_replace('#([/^$])#', '\\\\$1', $route['path']) . '$/';
            $this->router->register(new Route(
                    $regex,
                    $route['controller'],
                    $route['action'],
                    $route['request_method'] ?? RequestMethod::GET
                )
            );
        }
        $this->router->setFallback(new Route(
                $path = '',
                $config['routing']['fallback']['controller'],
                $config['routing']['fallback']['action'],
                RequestMethod::GET
            )
        );

        $db_config = json_decode(file_get_contents("{$directory}/{$config['database_config']}"), true);
        foreach ($db_config['database_connections'] as $con) {
            $db_con = new DatabaseConnection($con['dsn'], $con['username'], $con['password']);
            $this->database_connections[$con['name']] = $db_con;
        }

        $file_path = $directory . '/' . ($config['i18n']['lang_directory'] ?? "lang") . "/lang_{LANGUAGE}.json";
        $cache_path = $directory . '/' . ($config['i18n']['cache_directory'] ?? ".langcache/");
        $this->i18n = new i18n(
            $file_path,
            $cache_path,
            $config['i18n']['fallback_lang'] ?? "en"
        );
        /*if ($this->getEnv()->isDevelopment()) {*/
        // TODO: Only recompile in development and add script that does it in production
        $this->i18n->recompileIfOutdated();
        /*}*/
        $this->i18n->init();
    }

    /** @param callable $handler */
    public function registerExceptionHandler(callable $handler)
    {
        $this->exception_handlers[] = $handler;
    }

    /**
     * Initializes the remaining parts and handles the request.
     */
    public function run(): void
    {
        if ($this->getEnv()->usingOutputBuffering()) {
            ob_start();
        }

        try {
            // TODO: Create the Request object here
            $this->getRouter()->handleRequest(strtok($_SERVER['REQUEST_URI'], '?'));
        } catch (Throwable $exception) {
            foreach ($this->exception_handlers as $handler) {
                $handler($exception);
            }
        }
    }

    /** @return Router */
    public function getRouter(): Router
    {
        return $this->router;
    }

    /** @return DependencyInjectionService */
    public function getDependencyInjectionService() : DependencyInjectionService
    {
        return $this->dependency_injection_service;
    }

    /** @return DatabaseConnection[] */
    public function getDatabaseConnections(): array
    {
        return $this->database_connections;
    }

    /** @return i18n */
    public function getI18n(): i18n
    {
        return $this->i18n;
    }
}
