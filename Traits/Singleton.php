<?php

namespace Framework\Traits;

/**
 * Trait Singleton
 * @package Framework\Traits
 */
trait Singleton
{
    /** @var static */
    protected static self $instance;

    /** @return static */
    public static function getInstance(): self
    {
        if (!isset(self::$instance) || self::$instance === null) {
            self::$instance = static::createInstance();
        }

        return self::$instance;
    }

    /** @return static */
    protected static function createInstance(): self
    {
        return new static();
    }
}
