<?php

namespace Framework;

use ArrayAccess;
use Exception;
use InvalidArgumentException;

/**
 * Class Utils
 * @package Framework
 */
class Utils
{
    private function __construct()
    {
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateCryptoString($length = 32): string
    {
        try {
            return bin2hex(random_bytes($length));
        } catch (Exception $exception) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }

    /** @return string */
    public static function getBacktrace(): string
    {
        ob_start();
        debug_print_backtrace();
        $trace = ob_get_contents();
        ob_end_clean();

        return $trace ?? "";
    }

    /** @return string */
    public static function getRequestMethod(): string
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @param ArrayAccess|array $array
     * @param string $arrayKey
     * @param mixed $value
     */
    public static function arrayPushOrCreate(ArrayAccess|array &$array, string $arrayKey, $value): void
    {
        if (isset($array[$arrayKey])) {
            if (is_array($array[$arrayKey])) {
                $array[$arrayKey][] = $value;
            } else {
                $array[$arrayKey] = [$array[$arrayKey], $value];
            }
        } else {
            $array[$arrayKey] = [$value];
        }
    }

    /**
     * @param array $array
     * @return array
     */
    public static function arrayFlatten(array $array): array
    {
        $return = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $return = array_merge($return, Utils::arrayFlatten($value));
            } else {
                $return[$key] = $value;
            }
        }

        return $return;
    }

    /**
     * @param mixed $obj
     * @param bool $write
     * @return string|null
     */
    public static function dump($obj, bool $write = true): ?string
    {
        ob_start();
        var_dump($obj);
        $contents = ob_get_clean();
        if ($write) {
            echo "<pre><code>$contents</code></pre>";
        }

        return $write ? null : $contents;
    }

    /**
     * @param string $left_type
     * @param string $right_type
     * @return bool
     */
    public static function typeNameEquals(string $left_type, string $right_type): bool
    {
        static $type_array = [
            'bool' => 0, 'boolean' => 0,
            'int' => 1, 'integer' => 1,
            'float' => 2, 'double' => 2,
            'string' => 3, 'str' => 3,
            'obj' => 4, 'object' => 4,
        ];

        return $type_array[$left_type] === $type_array[$right_type];
    }

    /**
     * @param mixed $obj
     * @param string $type_name
     * @return bool|float|int|string
     * @throws InvalidArgumentException
     */
    public static function castToPrimitive($obj, string $type_name): int|bool|float|string
    {
        switch ($type_name) {
            case 'bool':
            case 'boolean':
                return (bool)$obj;
            case 'int':
            case 'integer':
                return (int)$obj;
            case 'float':
            case 'double':
                return (float)$obj;
            case 'string':
            case 'str':
                return (string)$obj;
            default:
                throw new InvalidArgumentException("\$obj can't be cast to ${type_name}.");
        }
    }

    /**
     * @return bool
     */
    public static function isSSL(): bool
    {
        if (isset($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS'])) {
            return true;
        }

        if (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }

        return false;
    }
}
