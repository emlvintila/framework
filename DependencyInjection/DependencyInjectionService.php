<?php

namespace Framework\DependencyInjection;

use Framework\Utils;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Class DependencyInjectionService
 * @package Framework\DependencyInjection
 */
class DependencyInjectionService
{
    /** @var array */
    protected array $objects = [];

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param object $obj
     * @return DependencyInjectionService
     */
    public function register(object $obj): DependencyInjectionService
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $class = new ReflectionClass($obj);
        $name = $class->getName();
        /*if (array_key_exists($name, $this->objects)) {
            throw new InvalidArgumentException("Can't register more than one $name for dependency injection.");
        }*/
        $this->objects[$name] = &$obj;
        foreach ($class->getInterfaceNames() as $interface_name) {
            $this->objects[$interface_name] = &$obj;
        }
        for ($parent = $class->getParentClass(); $parent !== false; $parent = $parent->getParentClass()) {
            $this->objects[$parent->getName()] = &$obj;
        }

        return $this;
    }

    /**
     * @param ReflectionClass $class
     * @return object The newly created instance of type $class
     */
    public function inject(ReflectionClass $class): object
    {
        $constructor = $class->getConstructor();
        $parameters = $constructor->getParameters();
        $args = [];
        foreach ($parameters as $parameter) {
            $parameter_class = $parameter->getClass();
            if (null === $parameter_class) {
                throw new InvalidArgumentException(
                    "The parameter \"{$parameter->getName()}\" in {$class->getName()}::{$constructor->getName()} is not type-hinted as an object.");
            }
            $instance = $this->getObject($parameter_class->getName());
            if (null === $instance) {
                $new_instance = $this->inject($parameter_class);
                $this->register($new_instance);
                $args[] = $new_instance;
            } else {
                $args[] = $instance;
            }
        }
        $args = Utils::arrayFlatten($args);

        return $class->newInstanceArgs($args);
    }

    /** @return array */
    public function getRegisteredObjects(): array
    {
        return $this->objects;
    }

    /**
     * @param string $type
     * @return mixed|null An object of type $type or null if there's no object registered
     */
    public function getObject(string $type): mixed
    {
        return $this->objects[$type] ?? null;
    }
}
