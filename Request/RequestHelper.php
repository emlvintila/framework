<?php

namespace Framework\Request;

use ReflectionClass;
use ReflectionException;

/**
 * Class RequestHelper
 */
class RequestHelper
{
    /**
     * @param string[] $request
     * @param string $class_name
     * @param bool $strict If strict is set to false, exceptions will be swallowed
     * @return object|null
     * @throws ReflectionException
     */
    public static function createEntityFromRequest(array $request, string $class_name, bool $strict = false): ?object
    {
        try {
            $class = new ReflectionClass($class_name);
        } catch (ReflectionException $exception) {
            if ($strict) {
                throw $exception;
            }
            return null;
        }
        $entity = $class->newInstanceWithoutConstructor();
        foreach ($request as $prop_name => $prop_value) {
            try {
                $ref_prop = $class->getProperty($prop_name);
                $ref_prop->setAccessible(true);
                $ref_prop->setValue($entity, $prop_value);
            } catch (ReflectionException $exception) {
                if ($strict) {
                    throw $exception;
                }
                continue;
            }
        }
        $constructor = $class->getConstructor();
        $constructor->invoke($entity);

        return $entity;
    }
}
