<?php

use Framework\View\View;

$view = new View('_base.php');
?>

<?php $view->beginSection('title') ?>
Hello, world!
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<h1>Hello, world!</h1>
<?php $view->endSection() ?>
