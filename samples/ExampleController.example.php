<?php

namespace App\Controller;

use Framework\Controller\ContentRenderResponse;
use Framework\Controller\Controller;
use Framework\Controller\ViewRenderResponse;

class ExampleController extends Controller
{
    public function __construct()
    {
    }

    public function helloAction(): ViewRenderResponse
    {
        return $this->renderView('home.php');
    }

    public function notFoundAction(): ContentRenderResponse
    {
        return $this->renderHtml("<h1>404</h1>", [
            $this->makeHttpResponseHeader(404),
        ]);
    }
}
