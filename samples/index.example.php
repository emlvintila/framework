<?php
require __DIR__ . '/../App/bootstrap.php';

use Framework\App;
use Framework\Logging\FileLogger;
use Framework\Logging\Logger;
use Framework\Persistence\Sql\Driver\GenericSqlDriver;
use Framework\Utils;

$app = App::getInstance();
setcookie(session_name(), session_id(), time() + 60 * 60 * 24 * 30, '/', '.example.com', true, true);

$logger = new FileLogger(__DIR__ . '/../logs/error.log');
$error_handler = function (int $log_severity, int $http_code, ...$args) use ($logger) {
    http_response_code($http_code);
    if (App::isDevelopment()) {
        Utils::dump($args);
    } else {
        $logger->log($log_severity, $args);
    }
};

register_shutdown_function(function () use ($error_handler) {
    $error = error_get_last();
    if ($error !== null && $error['type'] === E_ERROR) {
        $error_handler(Logger::LOG_FATAL, 500, $error);
    }
});
$app->registerExceptionHandler(function (Throwable $throwable) use ($error_handler) {
    $error_handler(Logger::LOG_ERROR, 503, [$throwable->getMessage(), $throwable->getTraceAsString()]);
});

$app->configureFrom(__DIR__ . '/../config.json');

$connection = $app->getDatabaseConnections()['default'];
$sql_driver = new GenericSqlDriver($connection);
$app->getDependencyInjectionService()
    ->register($connection)
    ->register($sql_driver)
    ->register($logger);
$app->run();
