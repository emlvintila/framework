<?php

namespace Framework\Controller;

/**
 * Class ViewRenderResponse
 * @package Framework\Controller
 */
class ViewRenderResponse extends RenderResponse
{
    /** @var string */
    protected string $view_name;

    /**
     * @param string $view_name
     */
    public function __construct(string $view_name)
    {
        $this->view_name = $view_name;
    }

    /** @return string */
    public function getViewName(): string
    {
        return $this->view_name;
    }
}
