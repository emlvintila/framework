<?php

namespace Framework\Controller;

/**
 * Class ContentRenderResponse
 * @package Framework\Controller
 */
class ContentRenderResponse extends RenderResponse
{
    /** @var string */
    protected string $content;
    /** @var string|null */
    protected ?string $content_type;

    /**
     * @param string $content
     * @param string|null $content_type
     */
    public function __construct(string $content, ?string $content_type = null)
    {
        $this->content = $content;
        $this->content_type = $content_type;
    }

    /** @return string */
    public function getContent(): string
    {
        return $this->content;
    }

    /** @return string|null */
    public function getContentType(): ?string
    {
        return $this->content_type;
    }
}
