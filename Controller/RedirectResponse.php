<?php

namespace Framework\Controller;

/**
 * Class RedirectResponse
 * @package Framework\Controller
 */
class RedirectResponse extends Response
{
    /** @var string */
    protected string $uri;

    /**
     * @param string $uri
     */
    public function __construct(string $uri)
    {
        $this->uri = $uri;
    }

    /** @return string */
    public function getUri(): string
    {
        return $this->uri;
    }
}
