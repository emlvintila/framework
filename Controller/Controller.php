<?php

namespace Framework\Controller;

use Error;
use Framework\Session\Session;
use Framework\View\View;
use RuntimeException;

/**
 * Defines logic for rendering and interacting with views.
 * @package Framework
 */
abstract class Controller
{
    /** @var string */
    public const VIEWS_DIRECTORY = 'Views';
    /**
     * Relative to VIEWS_DIRECTORY
     * @var string
     */
    public const ERRORS_VIEWS_DIRECTORY = 'Errors';
    /** @var string[] */
    public const ERROR_VIEWS_EXTENSIONS = ['php', 'html'];
    /** @var string[] */
    public const HTTP_STATUS_CODES_MAP = [
        100 => "Continue",
        101 => "Switching Protocols",
        103 => "Early Hints",
        200 => "OK",
        201 => "Created",
        202 => "Accepted",
        203 => "Non-Authoritative Information",
        204 => "No Content",
        205 => "Reset Content",
        206 => "Partial Content",
        300 => "Multiple Choices",
        301 => "Moved Permanently",
        302 => "Found",
        303 => "See Other",
        304 => "Not Modified",
        307 => "Temporary Redirect",
        308 => "Permanent Redirect",
        400 => "Bad Request",
        401 => "Unauthorized",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Method Not Allowed",
        406 => "Not Acceptable",
        407 => "Proxy Authentication Required",
        408 => "Request Timeout",
        409 => "Conflict",
        410 => "Gone",
        411 => "Length Required",
        412 => "Precondition Failed",
        413 => "Payload Too Large",
        414 => "URI Too Long",
        415 => "Unsupported Media Type",
        416 => "Range Not Satisfiable",
        417 => "Expectation Failed",
        418 => "I'm a teapot",
        422 => "Unprocessable Entity",
        425 => "Too Early",
        426 => "Upgrade Required",
        428 => "Precondition Required",
        429 => "Too Many Requests",
        431 => "Request Header Fields Too Large",
        451 => "Unavailable For Legal Reasons",
        500 => "Internal Server Error",
        501 => "Not Implemented",
        502 => "Bad Gateway",
        503 => "Service Unavailable",
        504 => "Gateway Timeout",
        505 => "HTTP Version Not Supported",
        511 => "Network Authentication Required",
    ];

    /**
     * @return string
     */
    public static function getErrorViewsDir(): string
    {
        return static::getViewsDir() . DIRECTORY_SEPARATOR . static::ERRORS_VIEWS_DIRECTORY . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    public static function getViewsDir(): string
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/../' . static::VIEWS_DIRECTORY . '/';
    }

    /**
     * @param int $http_code
     * @return RenderResponse
     */
    final public function renderHttpStatusPage(int $http_code): RenderResponse
    {
        $files = array_filter(
            array_map(
                fn($extension) => static::ERRORS_VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . "{$http_code}.{$extension}",
                static::ERROR_VIEWS_EXTENSIONS
            ),
            fn($path) => stream_resolve_include_path(static::getViewsDir() . $path)
        );

        http_response_code($http_code);
        if (count($files) === 0) {
            return $this->renderHtml("<h1>{$http_code} {$this->httpCodeToString($http_code)}</h1>");
        }

        $view_file = $files[array_key_first($files)];
        return $this->renderView($view_file);
    }

    /**
     * @param string $html
     * @param array $headers
     * @return ContentRenderResponse
     */
    final public function renderHtml(string $html, array $headers = []): ContentRenderResponse
    {
        return $this->renderContent($html, 'text/html', $headers);
    }

    /**
     * @param string $content
     * @param string $content_type
     * @param array $headers
     * @return ContentRenderResponse
     */
    final public function renderContent(string $content, string $content_type,
        array $headers = []): ContentRenderResponse
    {
        $headers['Content-Type'] = $content_type;
        $this->sendHeaders($headers);
        echo $content;
        return new ContentRenderResponse($content, $content_type);
    }

    /**
     * Renders a view.
     * @param string $view_name The name of the view to render.
     * @param array $headers The headers to send to the client.
     * @return ViewRenderResponse
     */
    final public function renderView(string $view_name, array $headers = []): ViewRenderResponse
    {
        $this->sendHeaders($headers);
        /** @noinspection PhpIncludeInspection */
        require static::getViewsDir() . $view_name;

        try {
            View::getCurrent();
        } catch (Error $error) {
            throw new RuntimeException("$view_name is not a view.");
        }

        while (($base = View::getCurrent()->getBaseView()) !== null) {
            if ($base === $view_name) {
                throw new RuntimeException("$view_name cannot extend itself.");
            }

            /** @noinspection PhpIncludeInspection */
            require static::getViewsDir() . $base;
        }
        return new ViewRenderResponse($view_name);
    }

    /**
     * @param string $json
     * @param array $headers
     * @return ContentRenderResponse
     */
    final public function renderJson(string $json, array $headers = []): ContentRenderResponse
    {
        return $this->renderContent($json, 'application/json', $headers);
    }

    /**
     * @param string $xml
     * @param array $headers
     * @return ContentRenderResponse
     */
    final public function renderXml(string $xml, array $headers = []): ContentRenderResponse
    {
        return $this->renderContent($xml, 'application/xml', $headers);
    }

    /**
     * @param string $text
     * @param array $headers
     * @return ContentRenderResponse
     */
    final public function renderPlain(string $text, array $headers = []): ContentRenderResponse
    {
        return $this->renderContent($text, 'text/plain', $headers);
    }

    /** @return RedirectResponse */
    final public function reload(): RedirectResponse
    {
        return $this->redirectTo($_SERVER['REQUEST_URI'], 303);
    }

    /**
     * @param string $uri
     * @param int $http_code
     * @return RedirectResponse
     */
    final public function redirectTo(string $uri, $http_code = 302): RedirectResponse
    {
        http_response_code($http_code);
        $this->sendHeaders([
            'Location' => $uri,
        ]);

        return new RedirectResponse($uri);
    }

    /**
     * @param int $http_code
     * @return string|null
     */
    protected function httpCodeToString(int $http_code): ?string
    {
        return array_key_exists($http_code, static::HTTP_STATUS_CODES_MAP) ?
            static::HTTP_STATUS_CODES_MAP[$http_code] : null;
    }

    /** @param string[] $headers */
    protected function sendHeaders(array $headers): void
    {
        foreach ($headers as $key => $value) {
            header((is_numeric($key) ?: "$key: ") . $value);
        }
    }

    /**
     * @param string|null $value
     * @return bool Whether the CSRF token matches the one associated with the current session
     * @link https://en.wikipedia.org/wiki/Cross-site_request_forgery
     */
    protected function checkCsrfToken(string $value = null): bool
    {
        $value ??= $_REQUEST['csrf_token'] ?? null;

        return $value === Session::getInstance()->getCsrfToken()->get();
    }
}
