<?php
/** @noinspection PhpClassNamingConventionInspection */

namespace Framework\Tests\DependencyInjection;

use Framework\DependencyInjection\DependencyInjectionService;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Class DependencyInjectionServiceTest
 * @package Framework\Tests
 */
class DependencyInjectionServiceTest extends TestCase
{
    public function testRegister()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new Dependency());
        $this->assertContains($dependency, $di->getRegisteredObjects());
    }

    public function testRegisterChildClass()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new ChildDependency());
        $this->assertEquals($dependency, $di->getObject(ChildDependency::class));
        $this->assertEquals($dependency, $di->getObject(Dependency::class));
    }

    public function testRegisterInterface()
    {
        $di = new DependencyInjectionService();
        $di->register($interface = new InterfaceImpl());
        $this->assertEquals($interface, $di->getObject(IInterface::class));
    }

    public function testRegisterGetSameObjectBack()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new Dependency());
        $obj = $di->getObject(Dependency::class);
        $this->assertEquals($dependency, $obj);
    }

    public function testGetObjectNull()
    {
        $di = new DependencyInjectionService();
        $this->assertEquals(null, $di->getObject(Dependency::class));
    }

    public function testInjectSuccessful()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new Dependency());
        /** @var InjectableClass $instance */
        $instance = $di->inject(new ReflectionClass(InjectableClass::class));
        $this->assertInstanceOf(InjectableClass::class, $instance);
        $this->assertEquals($dependency, $instance->getDependency());
    }

    public function testInjectFailNoTypeHint()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new Dependency());
        $this->expectException(InvalidArgumentException::class);
        $di->inject(new ReflectionClass(NonInjectableClass::class));
    }

    public function testInjectCreateDependencyOnTheFly()
    {
        $di = new DependencyInjectionService();
        $di->register($dependency = new Dependency());
        /** @var InjectableClassThatRequiresOnTheFlyDependency $instance */
        $instance = $di->inject(new ReflectionClass(InjectableClassThatRequiresOnTheFlyDependency::class));
        $this->assertInstanceOf(InjectableClassThatRequiresOnTheFlyDependency::class, $instance);
        $this->assertNotNull($di->getObject(DependencyThatTakesADependency::class));
    }
}

/**
 * Class Dependency
 * @package Framework\Tests
 */
class Dependency
{
}

/**
 * Class ChildDependency
 * @package Framework\Tests
 */
class ChildDependency extends Dependency
{
}

/**
 * Class DependencyThatTakesADependency
 * @package Framework\Tests
 */
class DependencyThatTakesADependency
{
    /**
     * DependencyThatTakesADependency constructor.
     * @param Dependency $dependency
     */
    public function __construct(Dependency $dependency)
    {
    }
}

/**
 * Class InjectableClass
 * @package Framework\Tests
 */
class InjectableClass
{
    /** @var Dependency */
    private Dependency $dependency;

    /**
     * InjectableClass constructor.
     * @param Dependency $dependency
     */
    public function __construct(Dependency $dependency)
    {
        $this->dependency = $dependency;
    }

    /**
     * @return Dependency
     */
    public function getDependency(): Dependency
    {
        return $this->dependency;
    }
}

/**
 * Class NonInjectableClass
 * @package Framework\Tests
 */
class NonInjectableClass
{
    /**
     * NonInjectableClass constructor.
     * @param $dependency
     */
    public function __construct($dependency)
    {
    }
}

/**
 * Class InjectableClassThatRequiresOnTheFlyDependency
 * @package Framework\Tests
 */
class InjectableClassThatRequiresOnTheFlyDependency
{
    /**
     * InjectableClassThatRequiresOnTheFlyDependency constructor.
     * @param DependencyThatTakesADependency $dependency
     */
    public function __construct(DependencyThatTakesADependency $dependency)
    {
    }
}

/**
 * Interface IInterface
 * @package Framework\Tests
 */
interface IInterface
{
}

/**
 * Class InterfaceImpl
 * @package Framework\Tests
 */
class InterfaceImpl implements IInterface
{
}
