<?php
/** @noinspection PhpClassNamingConventionInspection */

namespace Framework;

/*
 * Fork this project on GitHub!
 * https://github.com/Philipp15b/php-i18n
 *
 * License: MIT
 */

use Exception;
use InvalidArgumentException;
use RuntimeException;

/**
 * Class i18n
 * @package Framework
 */
final class i18n
{
    /**
     * Language file path
     * This is the path for the language files. You must use the '{LANGUAGE}' placeholder for the language or the script wont find any language files.
     *
     * @var string
     */
    protected string $file_path = './lang/lang_{LANGUAGE}.ini';

    /**
     * Cache file path
     * This is the path for all the cache files. Best is an empty directory with no other files in it.
     *
     * @var string
     */
    protected string $cache_path = './langcache/';
    /**
     * Fallback language
     * This is the language which is used when there is no language file for all other user languages. It has the lowest priority.
     * Remember to create a language file for the fallback!!
     *
     * @var string
     */
    protected string $fallback_lang = 'en';
    /**
     * Merge in fallback language
     * Whether to merge current language's strings with the strings of the fallback language ($fallbackLang).
     *
     * @var bool
     */
    protected bool $merge_fallback = false;
    /**
     * The class name of the compiled class that contains the translated texts.
     * @var string
     */
    protected string $prefix = 'L';
    /**
     * Forced language
     * If you want to force a specific language define it here.
     *
     * @var string|null
     */
    protected ?string $forced_lang = null;

    /**
     * This is the separator used if you use sections in your ini-file.
     * For example, if you have a string 'greeting' in a section 'page' you can access it via 'L::page_greeting'.
     *
     * @var string
     */
    protected string $section_separator = '_';
    /**
     * User languages
     * These are the languages the user uses.
     * Normally, if you use the getUserLangs-method this array will be filled in like this:
     * 1. Forced language
     * 2. Language in $_GET['lang']
     * 3. Language in $_SESSION['lang']
     * 4. Fallback language
     *
     * @var array
     */
    protected array $user_langs = [];
    /**
     * @var string|null
     */
    protected ?string $applied_lang = null;
    /**
     * @var string|null
     */
    protected ?string $lang_file_path = null;
    /**
     * @var string|null
     */
    protected ?string $cache_file_path = null;

    /**
     * Constructor
     * The constructor sets all important settings. All params are optional, you can set the options via extra functions too.
     *
     * @param string $filePath This is the path for the language files. You must use the '{LANGUAGE}' placeholder for the language.
     * @param string $cachePath This is the path for all the cache files. Best is an empty directory with no other files in it. No placeholders.
     * @param string $fallbackLang This is the language which is used when there is no language file for all other user languages. It has the lowest priority.
     * @param bool $mergeFallback Whether to merge the current lang file and the fallback file if one of the translations is missing.
     * @param string $prefix The class name of the compiled class that contains the translated texts. Defaults to 'L'.
     */
    public function __construct(string $filePath, string $cachePath, string $fallbackLang, bool $mergeFallback = true, string $prefix = 'L')
    {
        $this->file_path = $filePath;
        $this->cache_path = $cachePath;
        $this->fallback_lang = $fallbackLang;
        $this->merge_fallback = $mergeFallback;
        $this->prefix = $prefix;

        $this->user_langs = $this->getUserLangsInternal();

        // search for language file
        foreach ($this->user_langs as $priority => $lang_code) {
            $this->lang_file_path = $this->getConfigFilename($lang_code);
            if (file_exists($this->lang_file_path)) {
                $this->applied_lang = $lang_code;
                break;
            }
        }
        if ($this->applied_lang === null) {
            throw new RuntimeException('No language file was found.');
        }

        // search for cache file
        $this->cache_file_path = $this->cache_path . '/php_i18n_' . md5_file(__FILE__) . '_' . $this->prefix . '_' . $this->applied_lang . '.cache.php';
    }

    /**
     * getUserLangs()
     * Returns the user languages
     * Normally it returns an array like this:
     * 1. Forced language
     * 2. Language in $_GET['lang']
     * 3. Language in $_SESSION['lang']
     * 4. HTTP_ACCEPT_LANGUAGE
     * 5. Fallback language
     * Note: duplicate values are deleted.
     *
     * @return array with the user languages sorted by priority.
     */
    protected function getUserLangsInternal(): array
    {
        $user_langs = [];

        // Highest priority: forced language
        if ($this->forced_lang != null) {
            $user_langs[] = $this->forced_lang;
        }

        // 2nd highest priority: GET parameter 'lang'
        if (isset($_GET['lang']) && is_string($_GET['lang'])) {
            $user_langs[] = $_GET['lang'];
        }

        // 3rd highest priority: SESSION parameter 'lang'
        if (isset($_SESSION['lang']) && is_string($_SESSION['lang'])) {
            $user_langs[] = $_SESSION['lang'];
        }

        // 4th highest priority: HTTP_ACCEPT_LANGUAGE
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            foreach (explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $part) {
                $user_langs[] = strtolower(substr($part, 0, 2));
            }
        }

        // Lowest priority: fallback
        $user_langs[] = $this->fallback_lang;

        // remove duplicate elements
        $user_langs = array_unique($user_langs);

        // remove illegal userLangs
        // only allow a-z, A-Z and 0-9 and _ and -
        return array_filter($user_langs, fn($value) => preg_match('/^[a-zA-Z0-9_-]*$/', $value) === 1);
    }

    /**
     * @param $lang_code
     * @return string|string[]
     */
    protected function getConfigFilename($lang_code): string|array
    {
        return str_replace('{LANGUAGE}', $lang_code, $this->file_path);
    }

    public function init()
    {
        /** @noinspection PhpIncludeInspection */
        require_once $this->cache_file_path;
    }

    /**
     * @throws Exception
     */
    public function recompileIfOutdated(): void
    {
        $outdated = !file_exists($this->cache_file_path) ||
            filemtime($this->cache_file_path) < filemtime($this->lang_file_path) || // the language config was updated
            ($this->merge_fallback && filemtime($this->cache_file_path) < filemtime($this->getConfigFilename($this->fallback_lang))); // the fallback language config was updated

        if ($outdated) {
            $this->recompile();
        }
    }

    /**
     * @throws Exception
     */
    public function recompile(): void
    {
        $config = $this->load($this->lang_file_path);
        if ($this->merge_fallback) {
            $config = array_replace_recursive($this->load($this->getConfigFilename($this->fallback_lang)), $config);
        }

        $compiled = <<<END
<?php

final class {$this->prefix}
{
{$this->compile($config)}
    public static function __callStatic(\$string, \$args) {
        return vsprintf(constant("self::\$string"), \$args);
    }
}

function {$this->prefix}(\$string, \$args = null)
{
    \$return = constant("{$this->prefix}::\$string");
    
    return \$args ? vsprintf(\$return, \$args) : \$return;
}
END;

        if (!is_dir($this->cache_path)) {
            mkdir($this->cache_path, 0755, true);
        }

        if (file_put_contents($this->cache_file_path, $compiled) === false) {
            throw new Exception("Could not write cache file to path '" . $this->cache_file_path . "'. Is it writable?");
        }
        chmod($this->cache_file_path, 0755);
    }

    /**
     * @param $filename
     * @return array|bool|mixed
     */
    protected function load($filename): mixed
    {
        $ext = substr(strrchr($filename, '.'), 1);
        switch ($ext) {
            case 'properties':
            case 'ini':
                $config = parse_ini_file($filename, true);
                break;
            case 'yml':
            case 'yaml':
                $config = yaml_parse_file($filename);
                break;
            case 'json':
                $config = json_decode(file_get_contents($filename), true);
                break;
            default:
                throw new InvalidArgumentException($ext . " is not a valid extension!");
        }
        return $config;
    }

    /**
     * Recursively compile an associative array to PHP code.
     * @param array $config
     * @param string $prefix
     * @param int $indent
     * @return string
     */
    protected function compile(array $config, string $prefix = '', int $indent = 4): string
    {
        $code = '';
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $code .= $this->compile($value, $prefix . $key . $this->section_separator);
            } else {
                $full_name = $prefix . $key;
                if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $full_name)) {
                    throw new InvalidArgumentException(__CLASS__ . ": Cannot compile translation key " .
                        $full_name . " because it is not a valid PHP identifier.");
                }
                $code .= str_repeat(' ', $indent) . 'const ' . $full_name . ' = \'' .
                    str_replace('\'', '\\\'', $value) . "';\n";
            }
        }

        return $code;
    }

    /** @return array */
    public function getUserLangs(): array
    {
        return $this->user_langs;
    }

    /**
     * @return string|null
     */
    public function getAppliedLang(): ?string
    {
        return $this->applied_lang;
    }

    /** @return string */
    public function getCachePath(): string
    {
        return $this->cache_path;
    }

    /** @return string */
    public function getFallbackLang(): string
    {
        return $this->fallback_lang;
    }
}
