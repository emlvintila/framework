<?php

namespace Framework\Persistence\Sql;

use Exception;

/**
 * Class SqlException
 * @package Framework\Persistence\Sql
 */
class SqlException extends Exception
{
    /** @var string */
    private string $sqlstate;

    /**
     * SqlException constructor.
     * @param string $sqlstate
     * @param int $error_code
     * @param string $error_message
     */
    public function __construct(string $sqlstate, int $error_code, string $error_message)
    {
        parent::__construct($error_message, $error_code);
        $this->sqlstate = $sqlstate;
    }

    /** @return string */
    public function getSqlstate(): string
    {
        return $this->sqlstate;
    }

    /**
     * String representation of the exception
     * @link https://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1
     */
    public function __toString(): string
    {
        return "SQLSTATE: {$this->getSqlstate()}\n" . parent::__toString();
    }
}
