<?php

namespace Framework\Persistence\Sql\Internal;

use Framework\Persistence\Sql\SqlException;
use Framework\Persistence\Sql\SqlResult;

/**
 * Interface IResultConsumer
 * @package Framework\Persistence\Sql\Internal
 */
interface IResultConsumer
{
    /**
     * @param SqlResult $result
     * @throws SqlException
     */
    public function __invoke(SqlResult $result);
}
