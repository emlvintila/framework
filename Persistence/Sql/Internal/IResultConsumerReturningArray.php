<?php

namespace Framework\Persistence\Sql\Internal;

use Framework\Persistence\Sql\SqlResult;

/**
 * Interface IResultConsumerReturningArray
 * @package Framework\Persistence\Sql\Internal
 */
interface IResultConsumerReturningArray extends IResultConsumer
{
    /**
     * @inheritDoc
     * @return array
     */
    public function __invoke(SqlResult $result): array;
}
