<?php

namespace Framework\Persistence\Sql\Internal;

use Framework\Persistence\Sql\SqlResult;

/**
 * Class ResultConsumerReturningObject
 * @package Framework\Persistence\Sql\Internal
 */
class ResultConsumerReturningObject implements IResultConsumerReturningObject
{
    /** @var string */
    private string $class_name;

    /**
     * constructor.
     * @param string $class_name
     */
    public function __construct(string $class_name)
    {
        $this->class_name = $class_name;
    }

    /** @inheritDoc */
    public function __invoke(SqlResult $result): ?object
    {
        return $result->getObject($this->class_name);
    }
}