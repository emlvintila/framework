<?php

namespace Framework\Persistence\Sql\Internal;

use Framework\Persistence\Sql\SqlException;
use Framework\Persistence\Sql\SqlResult;

/**
 * Class ResultConsumerReturningArray
 * @package Framework\Persistence\Sql\Internal
 */
class ResultConsumerReturningArray implements IResultConsumerReturningArray
{
    /**
     * @inheritDoc
     * @throws SqlException
     */
    public function __invoke(SqlResult $result): array
    {
        return $result->getRowAsArray();
    }
}
