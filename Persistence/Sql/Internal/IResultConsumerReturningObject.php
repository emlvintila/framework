<?php

namespace Framework\Persistence\Sql\Internal;

use Framework\Persistence\Sql\SqlResult;

/**
 * Interface IResultConsumerReturningObject
 * @package Framework\Persistence\Sql\Internal
 */
interface IResultConsumerReturningObject extends IResultConsumer
{
    /**
     * @inheritDoc
     * @return object|null
     */
    public function __invoke(SqlResult $result): ?object;
}