<?php

namespace Framework\Persistence\Sql\Driver;

use Framework\Persistence\Driver\IDriver;
use Framework\Persistence\Sql\SqlException;
use Framework\Persistence\Sql\SqlResult;

/**
 * Interface SqlDriver
 * @package Framework\Persistence\Sql\Driver
 */
interface ISqlDriver extends IDriver
{
    /**
     * @param string $query
     * @param array $params
     * @return SqlResult
     * @throws SqlException
     */
    public function executeQuery(string $query, array $params): SqlResult;

    /** @return int|null */
    public function getLastInsertId(): ?int;
}
