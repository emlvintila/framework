<?php

namespace Framework\Persistence\Sql\Driver;

use Framework\Persistence\Sql\DatabaseConnection;
use Framework\Persistence\Sql\SqlException;
use Framework\Persistence\Sql\SqlResult;
use PDOException;

/**
 * Class GenericSqlDriver
 * @package Framework\Persistence\Sql\Driver
 */
class GenericSqlDriver implements ISqlDriver
{
    /** @var DatabaseConnection */
    private DatabaseConnection $connection;

    /**
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @inheritDoc
     * @throws SqlException
     */
    public function executeQuery(string $query, array $params): SqlResult
    {
        $pdo = $this->connection->getPdo();
        $stmt = $pdo->prepare($query);
        if (false === $stmt) {
            [$sqlstate, $error_code, $error_message] = $pdo->errorInfo();
            throw new SqlException($sqlstate, $error_code, $error_message);
        }
        if (false === $stmt->execute($params)) {
            [$sqlstate, $error_code, $error_message] = $stmt->errorInfo();
            throw new SqlException($sqlstate, $error_code, $error_message);
        }

        return new SqlResult($stmt);
    }

    /**  @return int|null */
    public function getLastInsertId(): ?int
    {
        try {
            return $this->connection->getPdo()->lastInsertId();
        } catch (PDOException $exception) {
            return null;
        }
    }
}
