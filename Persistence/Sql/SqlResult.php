<?php

namespace Framework\Persistence\Sql;

use Framework\Persistence\Sql\Internal\IResultConsumer;
use Generator;
use PDO;
use PDOStatement;

/**
 * Class SqlResult
 * @package Framework\Persistence\Sql
 */
class SqlResult
{
    /** @var string */
    private const SQLSTATE_OK = '00000';

    /** @var PDOStatement */
    private PDOStatement $statement;

    /**
     * SqlResult constructor.
     * @param PDOStatement $statement
     */
    public function __construct(PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    /**
     * @param string $class_name
     * @return object|null
     * @throws SqlException
     */
    public function getObject(string $class_name): ?object
    {
        $object = $this->statement->fetchObject($class_name);
        return $this->returnValueOrNullThrowOnError($object);
    }

    /**
     * @param string $class_name
     * @return Generator
     * @throws SqlException
     */
    public function getAllObjectsLazy(string $class_name): Generator
    {
        return $this->generateAll(new Internal\ResultConsumerReturningObject($class_name));
    }

    /**
     * @param string $class_name
     * @return object[]
     * @throws SqlException
     */
    public function getAllObjects(string $class_name): array
    {
        return $this->iterateAll($this->getAllObjectsLazy($class_name));
    }

    /**
     * @return array
     * @throws SqlException
     */
    public function getRowAsArray(): array
    {
        $array = $this->statement->fetch(PDO::FETCH_ASSOC);
        return $this->returnValueOrNullThrowOnError($array);
    }

    /**
     * @return Generator
     * @throws SqlException
     */
    public function getAllRowsAsArraysLazy(): Generator
    {
        return $this->generateAll(new Internal\ResultConsumerReturningArray());
    }

    /**
     * @return array
     * @throws SqlException
     */
    public function getAllRowsAsArrays(): array
    {
        return $this->iterateAll($this->getAllRowsAsArraysLazy());
    }

    /**
     * @param $value
     * @return mixed|null
     * @throws SqlException
     */
    private function returnValueOrNullThrowOnError($value): ?mixed
    {
        if (false === $value) {
            [$sqlstate, $error_code, $error_message] = $this->statement->errorInfo();
            if ($sqlstate === self::SQLSTATE_OK) {
                return null;
            }

            throw new SqlException($sqlstate, $error_code, $error_message);
        }

        return $value;
    }

    /**
     * @param IResultConsumer $get_next_value
     * @return Generator
     * @throws SqlException
     */
    private function generateAll(IResultConsumer $get_next_value): Generator
    {
        while (true) {
            $value = $get_next_value($this);
            if (null === $value) {
                break;
            }
            yield $value;
        }
    }

    /**
     * @param Generator $generator
     * @return array
     */
    private function iterateAll(Generator $generator): array
    {
        $result = [];
        foreach ($generator as $value) {
            $result[] = $value;
        }

        return $result;
    }
}
