<?php

namespace Framework\Persistence\Sql;

use PDO;

/**
 * Class DatabaseConnection
 * @package Framework
 */
class DatabaseConnection
{
    /** @var string */
    protected string $dsn;
    /** @var string */
    protected string $username;
    /** @var string */
    protected string $password;
    /** @var PDO */
    protected PDO $pdo;

    /**
     * DatabaseConnection constructor.
     * @param string $dsn
     * @param string $username
     * @param string $password
     */
    public function __construct(string $dsn, string $username, string $password)
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
    }

    /** @param bool $throw */
    public function setThrowOnError(bool $throw)
    {
        $this->getPdo()->setAttribute(PDO::ATTR_ERRMODE, $throw ? PDO::ERRMODE_EXCEPTION : PDO::ERRMODE_SILENT);
    }

    /** @return PDO */
    public function getPdo(): PDO
    {
        if (!isset($this->pdo) || $this->pdo === null) {
            $this->pdo = new PDO($this->dsn, $this->username, $this->password, [
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT,
            ]);
        }

        return $this->pdo;
    }
}
