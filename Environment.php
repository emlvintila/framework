<?php

namespace Framework;

/**
 * Class Environment
 * @package Framework
 */
class Environment
{
    /** @var bool */
    protected bool $is_development = false;
    /** @var bool */
    protected bool $use_output_buffering;

    /**
     * Environment constructor.
     * @param bool $isDevelopment
     * @param bool $useOutputBuffering
     */
    public function __construct(bool $isDevelopment, bool $useOutputBuffering)
    {
        $this->is_development = $isDevelopment;
        $this->use_output_buffering = $useOutputBuffering;
    }

    /** @return bool */
    public function isDevelopment(): bool
    {
        return $this->is_development;
    }

    /** @return bool */
    public function usingOutputBuffering(): bool
    {
        return $this->use_output_buffering;
    }
}
